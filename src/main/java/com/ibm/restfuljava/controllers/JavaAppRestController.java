package com.ibm.restfuljava.controllers;

import java.time.Instant;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ibm.restfuljava.models.RestJavaApp;
import com.journeyxp.processor.logmanager.LogManager;
import com.journeyxp.processors.eventhandlers.LogEventHandler;

@RestController
public class JavaAppRestController {
	Gson gson = new Gson();
	private static LogEventHandler mLog = (new LogManager(JavaAppRestController.class)).getLogEventHandler();
    
	public JavaAppRestController() {

	}
	
	@RequestMapping
	(value = { "/json/corpusData" }, 
	method = { RequestMethod.GET }, 
	produces = { "application/json" })

	public ResponseEntity<String> corpusData() {

		RestJavaApp restJavaApp = new RestJavaApp();
		JsonObject responseJson = null;
		
		try {
			responseJson = restJavaApp.getCorpusData();
		}  catch (Exception e) {
			mLog.addErrorToLog(e.getMessage(), e);
			return new ResponseEntity<String>(this.catchException(e).toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(responseJson.toString(), HttpStatus.OK);
	}

	@RequestMapping
	(value = { "/json/corpusUpdate" }, 
	method = { RequestMethod.POST }, 
	produces = { "application/json" })

	public ResponseEntity<String> corpusUpdate(@RequestBody String requestJson) {
		
		if(requestJson == null){
			return new ResponseEntity<String>("Request is required", HttpStatus.BAD_REQUEST);
		}

		JsonObject request = (JsonObject) this.gson.fromJson(requestJson, JsonObject.class);
		
		if (request.has("data") && request.get("data").isJsonObject()) {
			request = request.getAsJsonObject("data");
		} else {
			return new ResponseEntity<String>("Data is missing and/or not a valid JSON object", HttpStatus.BAD_REQUEST);
		}

		RestJavaApp restJavaApp = new RestJavaApp();
		JsonObject responseJson = null;
		
		try {
			responseJson = restJavaApp.corpusUpdate(request);
		} catch (Exception e) {
			mLog.addErrorToLog(e.getMessage(), e);
			return new ResponseEntity<String>(this.catchException(e).toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(responseJson.toString(), HttpStatus.OK);
	}

	public JsonObject catchException(Exception e) {
		JsonObject response = new JsonObject();
		response.addProperty("reason", e.getMessage());
		response.addProperty("timestamp", Long.valueOf(Instant.now().toEpochMilli()));
		response.addProperty("error", e.getClass().getSimpleName());
		return response;
	}
}
