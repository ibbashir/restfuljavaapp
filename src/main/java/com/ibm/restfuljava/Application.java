package com.ibm.restfuljava;

import java.util.HashMap;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import com.ibm.restfuljava.util.Property;

/**
 * Created by Ibrahim on 10-10-2018.
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = { "com.ibm.restfuljava.*" })
public class Application {
	
	static HashMap<String, Object> properties = new HashMap<>();
	
	public static void main(String[] args) {
		try {
			properties.put("server.port", Property.getProperty("server.port"));
			properties.put("server.compression.enabled", true);
			properties.put("server.compression.mime-types", "application/json");
			properties.put("server.compression.min-response-size", 2048);
			
			new SpringApplicationBuilder().sources(Application.class)
		    							  .properties(properties)
		    							  .run(args);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
