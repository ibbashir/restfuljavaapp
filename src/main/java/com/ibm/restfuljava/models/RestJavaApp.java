package com.ibm.restfuljava.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ibm.restfuljava.handlers.CloudantDataHandler;
import com.ibm.restfuljava.responsestatus.ServiceResponse;
import com.ibm.restfuljava.responsestatus.StatusCode;
import com.journeyxp.processor.logmanager.LogManager;
import com.journeyxp.processors.eventhandlers.LogEventHandler;

public class RestJavaApp {
	private static LogEventHandler mLog = (new LogManager(RestJavaApp.class)).getLogEventHandler();

	public RestJavaApp() {

	}

	public JsonObject getCorpusData() {
		ServiceResponse response = null;
		JsonObject responseJO = new JsonObject();

		try {
			JsonArray records = null;
			CloudantDataHandler client = new CloudantDataHandler();

			records = client.getRecordsFromDB();
			client.shutDownClient();

			if (records != null) {
				responseJO.add("data", records);
				response = new ServiceResponse(StatusCode.STATUS_OK, "Corpus Data: ", responseJO);
			} else {
				response = new ServiceResponse(StatusCode.STATUS_NO_CONTENT, "Unable to fetch data from DB: ",
						responseJO);
			}

		} catch (Exception e) {
			String errorMessage = "";
			if (e.getMessage() != null) {
				errorMessage = e.getMessage();
			} else {
				errorMessage = e.toString();
			}
			mLog.addErrorToLog(errorMessage, e);
			response = new ServiceResponse(StatusCode.STATUS_INTERNAL_SERVER_ERROR, errorMessage, responseJO);
		}
		return response.generateResponse();
	}

	public JsonObject corpusUpdate(JsonObject request) {
		ServiceResponse response = null;
		JsonObject responseJO = new JsonObject();

		try {
			CloudantDataHandler client = new CloudantDataHandler();
			boolean isUpdate = false;

			if (request.has("_rev")) {
				isUpdate = true;
			}

			responseJO = client.saveRecordInDB(request, isUpdate);
			client.shutDownClient();

			if (responseJO != null) {
				response = new ServiceResponse(StatusCode.STATUS_OK, "Request processed: ", responseJO);
			} else {
				response = new ServiceResponse(StatusCode.STATUS_INTERNAL_SERVER_ERROR, "Unable to save record in DB: ",
						responseJO);
			}

		} catch (Exception e) {
			String errorMessage = "";
			if (e.getMessage() != null) {
				errorMessage = e.getMessage();
			} else {
				errorMessage = e.toString();
			}
			mLog.addErrorToLog(errorMessage, e);
			response = new ServiceResponse(StatusCode.STATUS_INTERNAL_SERVER_ERROR, errorMessage, responseJO);
		}
		return response.generateResponse();
	}
}
