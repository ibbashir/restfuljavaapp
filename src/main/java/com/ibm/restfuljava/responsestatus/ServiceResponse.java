package com.ibm.restfuljava.responsestatus;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

//import javax.ws.rs.NotFoundException;

import com.google.gson.JsonObject;

public class ServiceResponse {
	
	private StatusCode mCode;
	private String mDescription;
	private JsonObject mData;
	
	private static Map<StatusCode, StatusMessage> statusMap = new HashMap<>();
	
	
	public ServiceResponse(StatusCode code, String description, JsonObject data) {
		populateStatusMap();
		
		mCode = code;
		mDescription = description;
		mData = data;
		
		if (statusMap.containsKey(mCode) == false) {
			throw new NoSuchElementException("Status code '"+ code.getStatusCode() +"' not avaiable");
		}
	}

	private void populateStatusMap() {
		statusMap.put(StatusCode.STATUS_OK, StatusMessage.STATUS_200);
		statusMap.put(StatusCode.STATUS_CREATED, StatusMessage.STATUS_201);
		statusMap.put(StatusCode.STATUS_NO_CONTENT, StatusMessage.STATUS_204);
		statusMap.put(StatusCode.STATUS_BAD_REQUEST, StatusMessage.STATUS_400);
		statusMap.put(StatusCode.STATUS_NOT_FOUND, StatusMessage.STATUS_404);
		statusMap.put(StatusCode.STATUS_CONFLICT, StatusMessage.STATUS_409);
		statusMap.put(StatusCode.STATUS_INTERNAL_SERVER_ERROR, StatusMessage.STATUS_500);
		statusMap.put(StatusCode.STATUS_REQUEST_TIME_OUT, StatusMessage.STATUS_408);
		statusMap.put(StatusCode.STATUS_ERROR, StatusMessage.STATUS_0);
	}
	
	public JsonObject generateResponse() {
		JsonObject response = new JsonObject();
		
		JsonObject status = new JsonObject();
		status.addProperty("statusCode", mCode.getStatusCode());
		status.addProperty("statusMessage", statusMap.get(mCode).getStatusMessage());
		status.addProperty("statusDescription", mDescription);
		
		response.add("status", status);
		
		if (mData != null && mData.has("data")) {
			response.add("data", mData.get("data"));
		} else if (mData != null && mData.has("data") == false) {
			response.add("data", mData);
		}
		
		JsonObject finalResponse = new JsonObject();
		finalResponse.add("response", response);
		
		return finalResponse;
	}

}
