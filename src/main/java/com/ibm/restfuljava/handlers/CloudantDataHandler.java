package com.ibm.restfuljava.handlers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ibm.restfuljava.util.Property;

import journeyxp.com.cloudant.client.api.ClientBuilder;
import journeyxp.com.cloudant.client.api.CloudantClient;
import journeyxp.com.cloudant.client.api.Database;
import journeyxp.com.cloudant.client.api.model.Response;
import journeyxp.com.cloudant.client.org.lightcouch.DocumentConflictException;

public class CloudantDataHandler {
	private CloudantClient mClient;
	
	public CloudantDataHandler() {
		try {
			mClient = ClientBuilder.url(new URL(Property.getProperty(Property.COUCH_DB_URL) + ":" + Property.getProperty(Property.COUCH_DB_PORT)))
							.username(Property.getProperty(Property.COUCH_DB_USERNAME))
							.password(Property.getProperty(Property.COUCH_DB_PASS)).build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public JsonArray getRecordsFromDB() throws MalformedURLException {
		JsonArray records = new JsonArray();
		List<JsonObject> recordList = new ArrayList<>();
		
		Database db = mClient.database("records", true);
		try {
			recordList = db.getAllDocsRequestBuilder()
						.includeDocs(true)
						.build()
						.getResponse()
						.getDocsAs(JsonObject.class);
		} catch (IOException e) {
			
		}
		for(JsonObject record : recordList){
			records.add(record);
		}
		return records;
    }
	
	public JsonObject saveRecordInDB(JsonObject record, boolean isUpdate) throws MalformedURLException {
		JsonObject responseObj = new JsonObject();
		
		Database db = mClient.database("records", true);
		Response response = null;
		
		try{
			if(isUpdate){
				response = db.update(record);
			} else {
				response = db.save(record);	
			}
		} catch (DocumentConflictException e){
			responseObj.addProperty("status", 409);
			responseObj.addProperty("reason", "Document already exists with the same ID");
		}
		
		responseObj.addProperty("status", response.getStatusCode());
		responseObj.addProperty("id", response.getId());
		responseObj.addProperty("reason", response.getReason());
		
		return responseObj;
    }
	
	public void shutDownClient() {
    	try {
	    	if (mClient != null) {
	    		mClient.shutdown();
	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}

