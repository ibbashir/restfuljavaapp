package com.ibm.restfuljava.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Property {
	
	public static final String PROPERTIES_FILE_NAME = "restapp.properties";
	public static final String COUCH_DB_URL = "couchDbUrl";
	public static final String COUCH_DB_HOST = "couchDbHost";
	public static final String COUCH_DB_PORT = "couchDbPort";
	public static final String COUCH_DB_USERNAME = "couchDBUser";
	public static final String COUCH_DB_PASS = "couchDBPass";
	
	private static Properties prop = null;
	
	public static String getProperty(String propertyName) {
		if (prop == null) {
			prop = getProperties(PROPERTIES_FILE_NAME);
		}
		return prop.getProperty(propertyName);
		
	}

	public static Properties getProperties(String propertyFilePath) {
	
		if (prop == null) {
			prop = new Properties();
			InputStream input = null;
			try {
				input = new FileInputStream(propertyFilePath);
				prop.load(input);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return prop;
	}
	
	public static String getDesignDocumentsPath() {
		String path = "";
		path = getProperty("designDocumentsPath");
		
		if(path.endsWith("/")== false) {
			path = path + "/";
		}
		return path;
	}
}

